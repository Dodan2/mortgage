package com;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="homeLoan")
public class HomeLoan {
	@Id	

	private int id;
private double buildingsize;
private int pincode;
private double propArea;
private double propAge;
private double loanAmt;
private double salary;
private double loanTenure;
private double oldEmi;
private String myfile;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public double getBuildingsize() {
	return buildingsize;
}
public void setBuildingsize(double buildingsize) {
	this.buildingsize = buildingsize;
}
public int getPincode() {
	return pincode;
}
public void setPincode(int pincode) {
	this.pincode = pincode;
}
public double getPropArea() {
	return propArea;
}
public void setPropArea(double propArea) {
	this.propArea = propArea;
}
public double getPropAge() {
	return propAge;
}
public void setPropAge(double propAge) {
	this.propAge = propAge;
}
public double getLoanAmt() {
	return loanAmt;
}
public void setLoanAmt(double loanAmt) {
	this.loanAmt = loanAmt;
}
public double getSalary() {
	return salary;
}
public void setSalary(double salary) {
	this.salary = salary;
}
public double getLoanTenure() {
	return loanTenure;
}
public void setLoanTenure(double loanTenure) {
	this.loanTenure = loanTenure;
}
public double getOldEmi() {
	return oldEmi;
}
public void setOldEmi(double oldEmi) {
	this.oldEmi = oldEmi;
}
public String getMyfile() {
	return myfile;
}
public void setMyfile(String myfile) {
	this.myfile = myfile;
}
public HomeLoan() {
	super();
	// TODO Auto-generated constructor stub
}
public HomeLoan(int id, double buildingsize, int pincode, double propArea, double propAge, double loanAmt,
		double salary, double loanTenure, double oldEmi, String myfile) {
	super();
	this.id = id;
	this.buildingsize = buildingsize;
	this.pincode = pincode;
	this.propArea = propArea;
	this.propAge = propAge;
	this.loanAmt = loanAmt;
	this.salary = salary;
	this.loanTenure = loanTenure;
	this.oldEmi = oldEmi;
	this.myfile = myfile;
}



}
