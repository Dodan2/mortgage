package com;


import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface HomeLoanRepository extends JpaRepository<HomeLoan,Integer>,CrudRepository<HomeLoan,Integer> {

   
/*List<HomeLoan> findBypropAge(Double propAge);
   HomeLoan findBypropArea(Double propArea);
   HomeLoan findBybuildingsize(Double buildingsize);
   HomeLoan findByloanAmt(Double loanAmt);
   HomeLoan findByloanTenure(Double loanTen);
   HomeLoan findByoldEmi(Double oldEmi);
   HomeLoan findBysalary(Double salary);
   HomeLoan findBypincode(int pincode);*/
}
