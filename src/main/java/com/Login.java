package com;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="login_user")
public class Login {
	@Id
	
 private int id;

 private String userName;
 private long phoneNo;
 private String address;
 private long aadhar;
 private String pan;
 private String password;
 @DateTimeFormat(pattern="yyyy-MM-dd")
	

 private Date dob;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public long getPhoneNo() {
	return phoneNo;
}
public void setPhoneNo(long phoneNo) {
	this.phoneNo = phoneNo;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public long getAadhar() {
	return aadhar;
}
public void setAadhar(long aadhar) {
	this.aadhar = aadhar;
}
public String getPan() {
	return pan;
}
public void setPan(String pan) {
	this.pan = pan;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public Date getDob() {
	return dob;
}
public void setDob(Date dob) {
	this.dob = dob;
}
public Login(int id, String userName, long phoneNo, String address, long aadhar, String pan, String password,
		Date dob) {
	super();
	this.id = id;
	this.userName = userName;
	this.phoneNo = phoneNo;
	this.address = address;
	this.aadhar = aadhar;
	this.pan = pan;
	this.password = password;
	this.dob = dob;
}
public Login() {
	super();
	// TODO Auto-generated constructor stub
}
    

}
