package com;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;




@RestController
public class LoginController {
@Autowired
LoginRepository loginRepository;
@Autowired
HomeLoanRepository homeLoanRepository;
@CrossOrigin
@RequestMapping(value="/",method=RequestMethod.POST)
public ModelAndView login(@ModelAttribute("user") Login login){
	
	return new ModelAndView("login");
}
@CrossOrigin
 @RequestMapping(value="/registerUser",method=RequestMethod.POST)
 public ModelAndView register(@ModelAttribute("user")@Valid Login login,BindingResult bindingResults){
	 if(bindingResults.hasErrors())
		 return new ModelAndView("register");
	 loginRepository.save(login);	
	return new ModelAndView("success");
	 
 }
 @CrossOrigin
 @RequestMapping(value="/home")
 public ModelAndView home(HomeLoan homeLoan){
	 return new ModelAndView("raiseloan");
 }
 @RequestMapping(value="/homeLoan")
 public ModelAndView homeloan(HomeLoan homeLoan){
homeLoanRepository.save(homeLoan);	
	return new ModelAndView("success");
	 
 }
 
	 @CrossOrigin
	 	@RequestMapping(value = "/view") 
	 	    public ModelAndView viewAll() throws Exception{
	 	       
	 	List<Login> list = (List<Login>) loginRepository.findAll();
	 	          return new ModelAndView("success", "list", list);

	 	    } 
	  

 
 
 @RequestMapping(value = "/checkLogin",method=RequestMethod.POST)
 @CrossOrigin
 public Map<String, String> checkLogin(@RequestBody Login login, HttpSession session)
 {   
 Login login1 = loginRepository.findOne(login.getId());

 Map<String, String> obj = new HashMap<>();
 if(login1.getPassword().equals(login.getPassword()))
 {
 session.setAttribute("userName", login.getUserName());
       obj.put("status","success");
       obj.put("message","valid user");
 return obj;
 }
 else
 obj.put("status","success");
       obj.put("message","Invalid user");
       return obj;
 }
 @RequestMapping(value ="/home_calc",method=RequestMethod.POST)
 @CrossOrigin
 public Map<String, String> eval_home(@RequestBody HomeLoan homeloan, HttpServletRequest request,HttpSession session) {

	
       
     /*  int pincode=Integer.parseInt(request.getParameter("pincode"));
       int areaVal=0;
       double propAge = Double.parseDouble(request.getParameter("propAge"));
       double propArea = Double.parseDouble(request.getParameter("propArea"));
       double propCal=0;
       double buildingVal=0;
       double FinalPropVal=0;
       double Buildcal=0;
       double buildingsize = Double.parseDouble(request.getParameter("buildingsize"));
       double loanAmt = Double.parseDouble(request.getParameter("loanAmt"));
       double currEMI=0;
       double interest=0.08;
       double loanTenure = Double.parseDouble(request.getParameter("loanTenure"));
       double oldEmi = Double.parseDouble(request.getParameter("oldEmi"));
       double salary = Double.parseDouble(request.getParameter("salary"));*/
	/*HomeLoan homeloan1=homeLoanRepository.findBypropAge(homeloan.getPropAge());
	HomeLoan homeloan2=homeLoanRepository.findBypropArea(homeloan.getPropArea());
	HomeLoan homeloan3=homeLoanRepository.findBybuildingsize(homeloan.getBuildingsize());
	HomeLoan homeloan4=homeLoanRepository.findByloanAmt(homeloan.getLoanAmt());
	HomeLoan homeloan5=homeLoanRepository.findByloanTenure(homeloan.getLoanTenure());
	HomeLoan homeloan6=homeLoanRepository.findByoldEmi(homeloan.getOldEmi());
	HomeLoan homeloan8 =homeLoanRepository.findBysalary (homeloan.getSalary());
	HomeLoan homeloan9=homeLoanRepository.findBypincode(homeloan.getPincode());*/
	  /* double propAge=homeloan1.getPropAge();
	   double propArea =homeloan2.getPropArea();
	   double buildingsize =homeloan3.getBuildingsize();	
	   double loanAmt =homeloan4.getLoanAmt();
	     double loanTenure =homeloan5.getLoanTenure();
	     double oldEmi=homeloan6.getOldEmi();
	     double salary = homeloan7.getSalary();
	     int pincode=homeloan8.getPincode();*/
	 HomeLoan homeloan1=homeLoanRepository.findOne(homeloan.getId());
	  int areaVal=0;
	  double propCal=0;
	   double buildingVal=0;
       double FinalPropVal=0;
       double Buildcal=0;
       double currEMI=0;
       double interest=0.08;
       Map<String, String> obj = new HashMap<>();
       //property calculation 
      if(homeloan1.getPincode()==60021 || homeloan1.getPincode()==60022 ||homeloan1.getPincode()==60023 || homeloan1.getPincode()==60024)
       {
             areaVal=2000;
       }
       else
       {
             areaVal=1000;
       }
       propCal=areaVal*homeloan1.getPropArea();
       
       
 //Building Calculation
       if (homeloan1.getPropAge() <= 10 &&homeloan1.getPropAge()>=1) {
             buildingVal = 10;
       } else if ( homeloan1.getPropAge() >= 11 && homeloan1.getPropAge() <=20) {
             buildingVal = 5;
       }

       else {
            
             FinalPropVal = 0;
       }

       Buildcal = homeloan1.getBuildingsize()*buildingVal;
       System.out.println(Buildcal);
       //console.log("buildingCal : " + this.Buildcal);
       
       FinalPropVal=propCal+Buildcal;
       
       
       
currEMI=(((homeloan1.getLoanAmt())*(interest)*(1+interest)*homeloan1.getLoanTenure())/((1+interest)*((homeloan1.getLoanTenure())-1))); 
       
 double TotalEMI=currEMI+homeloan1.getOldEmi();
 
 if(TotalEMI< (60*homeloan1.getSalary())/100)
 {
       if(FinalPropVal>homeloan1.getLoanAmt())
       {
    	  
           obj.put("status","success");
           obj.put("message","approved");
     return obj;
       }
 }
       
       
       
 obj.put("status","success");
 obj.put("message","not approved");
return obj;
       
	
	
 }


}

