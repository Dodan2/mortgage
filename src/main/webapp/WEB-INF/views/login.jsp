<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New/Edit Contact</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<div class="header">
<br>
<h4>Online Shopping Site</h4>
</div>
<div class= "jumbotron">
    <h3>Login Page</h3>
        <form:form action="checkLogin" method="" modelAttribute="user">
       <div class="form-group">
      <label for="emailId">EmailId:</label>
      <form:input path="userName" class="form-control" /><form:errors path="userName"></form:errors>
           </div>
           <div class="form-group">
      <label for="password">Password:</label>
      <form:input path="password" class="form-control" /><form:errors path="password"></form:errors>
           </div>
           <div>
           <button type="submit" class="btn btn-success">Login</button>
           <button type="reset" class="btn btn-info">Reset</button>
           </div>
           <div><br>
           Not an existing User? 
            <a href="/register">Sign up here</a>
           </div>
        </form:form>
    </div>
    </div>
    
</body>
</html>
