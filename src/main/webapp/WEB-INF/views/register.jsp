<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New/Edit Contact</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body background="/images/bg.jfif">
<div class="container">
<div class="header">
<br>
</div>
<div class= "jumbotron">
    <h3>Register Here!! </h3>
        <form:form action="/registerUser" method="get" modelAttribute="user">
       <div class="form-group">
    <label for="userName">Email:</label>
    <form:input path="userName" class="form-control" /><form:errors path="userName"></form:errors>
  </div>
  <div class="form-group">
    <label for="phoneNo">Name:</label>
    <form:input path="phoneNo" class="form-control" /><form:errors path="phoneNo"></form:errors>
  </div>
  <div class="form-group">
    <label for="address">Contact No:</label>
    <form:input path="address" class="form-control" /><form:errors path="address"></form:errors>
  </div>
  <div class="form-group">
    <label for="aadhar">Address:</label>
    <form:input path="aadhar" class="form-control" /><form:errors path="aadhar"></form:errors>
  </div>
  <div class="form-group">
    <label for="pan">Gender:</label>
    <form:input path="pan" class="form-control" /><form:errors path="pan"></form:errors>
  </div>
  <div class="form-group">
    <label for="password">Gender:</label>
    <form:input path="password" class="form-control" /><form:errors path="password"></form:errors>
  </div>
  <div class="form-group">
    <label for="dob">DOB:</label>
    <form:input type="date" path="dob" class="form-control"/><form:errors path="dob"></form:errors>
  </div>
  <div>
  <button type="submit" class="btn btn-success">Sign Up!</button>
<!--   <button type="reset" class="btn btn-info">Reset</button> -->
  </div>
        </form:form>
    </div>
    </div>
    
</body>
</html>
